#!/usr/bin/env python
#encoding=utf-8

from libs.base import *
import time
import tornado.web

class indexHandler(BaseHandler):#tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")

class contactHandler(BaseHandler):#tornado.web.RequestHandler):
    def get(self):
        self.render("contact.html")

class miscHandler(BaseHandler):
    def get(self,misc):
        self.render(misc+".html")

class successHandler(BaseHandler):#tornado.web.RequestHandler):
    def get(self,token):
        info = self.db.query("SELECT * FROM timecard WHERE token = %s", token)
        if info:
            self.render("success.html",
                fname=info[0]['first_name'],
                lname=info[0]['last_name'],
                app_date=info[0]['app_date'],
                app_time=info[0]['app_time'],
                token=token)
        else:
            self.render("index.html")

class mailHandler(BaseHandler):
    def get(self):
        token = self.get_argument("token","")
        if token == "a57a23f2b1340cf0f7fa1fb7ec1e9056":
            info = self.query("SELECT * FROM timecard WHERE read = 0;")
            self.execute("UPDATE timecard SET read = 1 WHERE read = 0;")
            self.render("admin.html",info=info)
        else:
            self.reader("index.html")


class loginHandler(BaseHandler):#tornado.web.RequestHandler):
    def get(self):
        self.render("login.html")

class registerHandler(BaseHandler):#tornado.web.RequestHandler):
    def get(self):
        self.render("register.html")

class errorHandler(BaseHandler):
    def get(self):
        self.render("index.html")

class adminHandler(BaseHandler):#tornado.web.RequestHandler):
    def get(self):
        self.render("admin.html")

class share(tornado.web.UIModule):
    def render(self):
        return self.render_string("module-share.html")
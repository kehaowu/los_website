#!/usr/bin/env python
import re
import os.path,os

class markdown():
    def __init__(self,app):
        self.tmplate_path = app.settings['template_path']
        self.post_path = "_post/"
        self.layout_path = "_post/_layout/"
        self.files = os.listdir(self.post_path)
        restring = r"(md)|(markdown)"
        for filename in self.files:
            if re.search(restring,filename):
                self.render(filename)
    def render(self,filename):
        file = open("".join([self.post_path,filename]),"r")
        contents = file.readlines()
        newcontents = []
        settings = {}
        headerflag = False
        self.tableflag = False
        if contents[0].strip() == "---":
            headerflag = True
            del contents[0]
        for content in contents:
             if headerflag == False:
                 content = content.strip()
                 content = self.sharpinterpreter(content)
                 content = self.imginterpreter(content)
                 content = self.hrefinterpreter(content)
                 content = self.tableinterpreter(content)
                 content = self.bookmarkinterpreter(content)
                 content = self.pinterpreter(content)
                 newcontents.append(content)
             else:
                 if content.strip() == "---":
                     headerflag = False
                     continue
                 temparray = content.split(":")
                 if len(temparray) == 2:
                     settings[temparray[0].replace("\s+","")] = temparray[1].strip().replace("\s+","")
                 del temparray
        file.close()
        self.layout_file = "".join([self.layout_path,settings['layout']])
        self.out_file = "".join([self.tmplate_path,filename.replace("markdown","html").replace("md","html")])
        if not os.path.isfile(self.layout_file):
            self.layout_file = "".join([self.layout_path,"default.html"])
        file = open(self.layout_file,"r")
        outfile = open(self.out_file,"w")
        restring = r"\{markdown%\s+%markdown\}"
        for content in file.readlines():
            content = content.strip()
            if re.search(restring,content):
                outfile.write("\n".join(newcontents))
            else:
                outfile.write(content+"\n")
        print "Generating " + self.out_file + "... ..."
        file.close()
        outfile.close()
    def sharpinterpreter(self,sentence):
        for i in '654321':
            rsharp = r"^"+"".join(["#{",i,"}"])
            if re.search(rsharp,sentence) :
                return "".join([sentence.strip().replace("".join(["#"]*int(i)),"".join(["<h",i,">"])),"".join(["</h",i,">"])])
        return sentence
    def imginterpreter(self,sentence):
        rimg = r"!\[.+\]\(.+\)"
        if re.search(rimg,sentence) :
            rehttp = r"(http://)|(https://)"
            if re.search(rehttp,sentence):
                return sentence.replace("![","<img alt='").replace("]","'").replace("("," src='").replace(")","'>").replace("\"","").replace("'","")
            else:
                return sentence.replace("\"","").replace("'","").replace("![","<img alt='").replace("]","'").replace("("," src=\"{{ static_url('").replace(")","') }}\">")
        return sentence
    def hrefinterpreter(self,sentence):
        rhref = r"\[.+\]\(.+\)"
        if re.search(rhref,sentence) :
            strarray = sentence.replace("\\","\\\\")
            strarray = re.split(r"\[|\]\(|\)",strarray)

            return strarray[0] + "<a href='" + strarray[2] + "'>" + strarray[1] + "</a>" + strarray[3]
        return sentence
    def pinterpreter(self,sentence):
        rp = r"^<"
        if not re.search(rp,sentence):
            return "".join(["<p>",sentence,"</p>"])
        else:
            return sentence
    def bookmarkinterpreter(self,sentence):
        if re.search("---bookmark",sentence):
            return "".join(["<p id='",sentence.split(" ")[1],"'></p>"])
        else:
            return sentence
    def tableinterpreter(self,sentence):
        if sentence.strip() == "---table-start":
            self.tableflag = True
            return "<table class=\"table table-striped table-hover\">"
        if sentence.strip() == "---table-end":
            self.tableflag = False
            return "</table>"
        if self.tableflag :
            retableheader = r"^---table-head"
            if re.search(retableheader,sentence):
                sentence = sentence.replace("---table-head","")
                label = "<th>"
                labelend = "</th>"
            else:
                label = "<td>"
                labelend = "</td>"
            unitarray = sentence.split("\t|\t")
            for i in range(len(unitarray)):
                unitarray[i] = "".join([label,unitarray[i],labelend])
            sentence = "".join(["<tr>","".join(unitarray),"</tr>"])
            return sentence
        else:
            return sentence

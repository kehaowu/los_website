import json
from O365 import *

class mail():
    def __init__(self):
        authconfig = json.load(file("config/server.cfg"))
        authenticiation = (authconfig['O365']['email'],authconfig['O365']['password'])
        self.m = Message(auth=authenticiation)
    def set_body(self, body):
        self.body = body
    def set_subject(self, subject):
        self.subject = subject
    def sendmail(self, mailto):
        self.m.setRecipients(mailto)
        self.m.setSubject(self.subject)
        self.m.setBody(self.body)
        self.m.sendMessage()
        print "Success."
#!/usr/bin/env python

import tornado.options
import tornado.ioloop
import tornado.web
import torndb
import urlparse,json
from time import ctime,strptime

timezone = 5

class BaseHandler(tornado.web.RequestHandler):

    @property
    def db(self):
        return self.application.db

    def getAvailableTime(self,wday):
        wday = wday + 1
        if wday == 5 :
            return  [
                (9,0), (10,0)
            ]
        else:
            return [
                (9,0), (10,0),
                (10,30),(13,0),
                (14,0), (14,30)
            ]
    def checkHoliday(self,date):
        holidays = json.load(file("config/holiday.cfg"))['holiday']
        holidaySet = []
        for holiday in holidays:
            holidaySet.append(strptime(holidays[holiday],"%m/%d/%Y"))
        return date in holidaySet
    def screen(self,string):
        print "\033[96m",ctime(),string,"\033[0m"
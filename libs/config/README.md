# GetConfig

    class: MysqlConfig(dbID)
\tRead config/mysql.cfg file, return an instance contains unmodifieable property:
instance.all:\tall config in the config file
instance.host:\thost property of dbID
instance.user:\tuser property of dbID
instance.passwd:\tpasswd property of dbID
instance.port:\tport property of dbID
instance.database:\tdatabase property of dbID
instance.charset:\tcharset property of dbID
instance.time_zone:\ttime_zone property of dbID
---
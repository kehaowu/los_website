import datetime, time, re, json, os
from O365 import *
from html2text import html2text

class patient():
    def __init__(self):
        authconfig = json.load(file("config/server.cfg"))['O365']
        self.authenticiation = (authconfig['email'],authconfig['password'])
        self.time_string = '%Y-%m-%dT%H:%M:%SZ'
        self.date_string = '%Y-%m-%d'
        self.start_date = None
        self.end_date = None
        self.sms = True
    def __check_date_format(self,date):
        return not re.search(r'^\d{4}-\d{2}-\d{2}$',date) is None
    def __start_today(self):
        return datetime.datetime.strftime(datetime.datetime.now(), self.date_string) + "T00:00:00Z"
    def __end_today(self):
        return datetime.datetime.strftime(datetime.datetime.now(), self.date_string) + "T23:59:59Z"
    def __start_tomorrow(self):
        return datetime.datetime.strftime(datetime.date.today() + datetime.timedelta(days=2), self.date_string) + "T00:00:00Z"
    def __end_tomorrow(self):
        return datetime.datetime.strftime(datetime.date.today() + datetime.timedelta(days=2), self.date_string) + "T23:59:59Z"
    def set_date(self,date=None):
        if date is None:
            self.start_date = self.__start_today()
            self.end_date = self.__end_today()
        elif date == 1:
            self.start_date = self.__start_tomorrow()
            self.end_date = self.__end_tomorrow()
        else:
            if self.__check_date_format(date):
                self.start_date = date + "T00:00:00Z"
                self.end_date = date + "T23:59:59Z"                
            else:
                self.start_date = self.__start_today()
                self.end_date = self.__end_today()
    def set_sms(self,sms = True):
        if sms is True:
            self.sms = True
        else:
            self.sms = False
    def fetch(self):
        if self.start_date is None:
            self.start_date = self.__start_today()
            self.end_date = self.__end_today()
        schedule = Schedule(self.authenticiation)
        result = schedule.getCalendars()
        cal = schedule.calendars[0]
        result = cal.getEvents(self.start_date,self.end_date)
        patients = []
        for event in cal.events:
            eventJson = event.fullcalendarioJson()
            showup = eventJson['showup']
            category = ','.join(eventJson['categories'])
            comment = html2text(eventJson['body'])
            commenthtml = eventJson['body']
            datetime1 = eventJson['start']
            pattern = re.compile(r'(?P<date>\d{4}-\d{2}-\d{2})T(?P<time>\d{2}:\d{2}:\d{2})')
            match = pattern.search(datetime1)
            date = match.group('date')
            apptime = match.group('time')
            apptime2 = datetime.datetime.strptime(apptime,'%H:%M:%S') - datetime.timedelta(hours=5)
            apptime = datetime.datetime.strftime(apptime2, '%H:%M ')
            if self.sms is True:
                pattern = re.compile(r'(?P<name>.+?)\s*(?P<tel>\d{3}.*?\d{3}.*?\d{4})\s*(?P<sms>Y|y)')
            else:
                pattern = re.compile(r'(?P<name>.+?)\s*(?P<tel>\d{3}.*?\d{3}.*?\d{4})')
            match = pattern.search(eventJson['title'])
            if not match is None:
                name = match.group('name')
                tel = match.group('tel')
                sms = False
                if self.sms is True:
                    if not match.group('sms') is None:
                        sms = True
                    else:
                        sms = False
                else:
                    sms = False
                tel = re.sub(r'\D','',tel)
                patient = {
                    'date': date,
                    'time':apptime,
                    'name': name,
                    'tel': tel,
                    'category': category,
                    'showup': showup,
                    'comment': re.sub(r'\n',';',comment),
                    'html': commenthtml,
                    'sms': sms
                }
                patients.append(patient)
        self.patients = patients
    def ifnotcreate(self):
        dirset = self.filename.split("/")
        dirset.pop()
        for i in range(len(dirset)):
            dir = '/'.join(dirset[0:(i+1)])
            if not os.path.exists(dir):
                print "Creating "+ dir
                os.makedirs(dir)
    def write(self,filename=None):
        if filename is None:
            tmp = ["patientlog/calendars.fetch",self.start_date,self.end_date,"txt"]
            filename = '.'.join(tmp)
            self.filename = re.sub(r':','',filename)
        self.ifnotcreate()
        f = open(self.filename,"w")
        f.write("Date\tName\ttel\tshowup\tcategory\tcomment\n")
        for patient in self.patients:
            f.write('\t'.join([\
                patient['date'],\
                patient['name'].encode('ascii', 'ignore').decode('ascii'),\
                patient['tel'],\
                str(patient['showup']),\
                patient['category'],\
                patient['comment'].encode('ascii', 'ignore').decode('ascii'),\
                "\n"
                ]))
        f.close()





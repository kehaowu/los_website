        if ($(window).height() > $(".jumbotron").height()){
            var upper = ($(window).height() - $(".jumbotron").height()) * 0.3 + parseInt($(".jumbotron").css("padding-top").split("px")[0])
            var lower = ($(window).height() - $(".jumbotron").height()) * 0.7 + parseInt($(".jumbotron").css("padding-bottom").split("px")[0])
            $(".jumbotron").css("padding-top",upper+"px")
            $(".jumbotron").css("padding-bottom",lower+"px")
        }
        $(function() {
            validator = $("#appointment").validate({
                rules:{
                    'appointment-date':{
                        required:true
                    },
                    'appointment-time':{
                        required:true
                    },
                    'first-name':{
                        required:true,
                        minlength:2,
                        maxlength:100
                    },
                    'last-name':{
                        required:true,
                        minlength:2,
                        maxlength:100
                    },
                    'phone':{
                        required:true,
                        minlength:10,
                        maxlength:20
                    },
                    'email':{
                        required:false
                    },
                    'read-me':{
                        required:true
                    }
                },
                messages:{
                    'appointment-date':{
                        required:"Please select date.",
                        remote:{
                            url:"/api/availabledate",
                            type:"GET",
                            dataType:"json",
                            data:{
                                'date':moment($("#appointment-date").val()).format("YYYY-MM-DD")
                            }
                        }
                    },
                    'appointment-time':{
                        required:"Please select time"
                    },
                    'first-name':{
                        required:"Please input your first name",
                        minlength:"Minimium length is 2"
                    },
                    'last-name':{
                        required:"Please input your last name",
                        minlength:"Minimium length is 2"
                    },
                    'phone':{
                        required:"Please input your phone number"
                    },
                    'email':{
                        required:"Please input your e-mail"
                    },
                    'read-me':{
                        required:"You can't make appointment until you read and agree with this."
                    }
                },
                errorPlacement: function(label, element) {
                    label.addClass('text-danger');
                    label.insertAfter(element);
                },
            })
        })
        $(function() {
            $("#reset").click(function(){
                $("#appointment")[0].reset()
            });
            $("#read-me-father").click(function(){
                $("#read-me").modal("show");
                $("#read-me-father").unbind("click");
                $(".read-me").click(function(){
                    $("#read-me").modal("show")
                });
            });
            $("#appointment-date").datetimepicker({
                timepicker:false,
                format:'m/d/Y',
                onGenerate:function( ct ){
                    jQuery(this).find('.xdsoft_date.xdsoft_weekend')
                    .addClass('xdsoft_disabled');
                },
                minDate:moment().add(86400,"seconds").format("YYYY/MM/DD"),
                startDate:moment().add(86400,"seconds").format("YYYY/MM/DD")
            });
            $("#appointment-date").change(function(){
                $("#appointment-time").attr("placeholder","Select time please.").removeAttr("disabled")
            });
            $("#appointment-date").blur(function(){
                if($("#appointment-date").val()==""){
                    $("#appointment-date").attr("placeholder","Please select a valid date.")
                    $("#processing-info").html("Please select a valid date.")
                    $("#processing").modal("show")
                    setTimeout(function() {
                        $("#processing").modal("hide")
                    }, 3000);
                    return
                }
                $("#processing-info").html("Searching for available time ... ... please wait for a moment ... ... ")
                $("#processing").modal("show")
                $("#appointment-time").attr("disabled","disabled").attr("placeholder","Searching for available time ... ...")
                $.ajax({
                    url:"/api/availabletime",
                    type:"GET",
                    dataType:"json",
                    data:{
                        'date':moment($("#appointment-date").val()).format("YYYY-MM-DD")
                    },
                    success:function(dat){
                        $("#processing-info").html("")
                        $("#processing").modal("hide")
                        if(dat.code==1001){
                            if(dat.data.length==0){
                                $("#appointment-time").val("").attr("placeholder","No time is available.")
                            }else{
                                $("#appointment-time").removeAttr("disabled").attr("placeholder","select time").val(dat.data[0])
                                $("#appointment-time").datetimepicker({
                                      datepicker:false,
                                      allowTimes:dat.data,
                                      format:'H:i'
                                })
                            }
                        }else{
                            $("#appointment-time").attr("disabled","disabled").val("").attr("placeholder",dat.message)
                        }
                    }
                })
            })
            $("#submit").click(function(){
                reg = /\d\d:\d\d/
                if (!reg.test($("#appointment-time").val())){
                    $("#appointment-time").attr("placeholder","Please input correct date")
                    return
                }
                $(".processing").css("display","inline")
                var valid = $("#appointment").valid()
                if (valid){
                    $("#confirm-body").html("Do you really want to make an appointment at "+$("#appointment-time").val() + " on " +moment($("#appointment-date").val()).format("YYYY-MM-DD") + "?" )
                    $("#confirm-modal").modal("show")
                }
            });
            $("#cancel-button").click(function(){
                $("#confirm-modal").modal("hide")
            })
            $("#confirm-button").click(function(){
                $("#confirm-modal").modal("hide")
                var valid = $("#appointment").valid()
                if(valid){
                    $("#processing-info").html("We are processing your request ... ...")
                    $("#processing").modal("show")
                    $("#submit").html("Processing ... ... ")
                    $.ajax({
                        url:'/api/makeappointment',
                        type:'post',
                        dataType:'json',
                        data:{
                            'dat':JSON.stringify({
                                date:moment($("#appointment-date").val()).format("YYYY-MM-DD"),
                                time:$("#appointment-time").val(),
                                fname:$("#first-name").val(),
                                lname:$("#last-name").val(),
                                dob:moment("1990-01-0").format("YYYY-MM-DD"),
                                phone:$("#phone").val(),
                                email:$("#email").val(),
                                mobile:$("input.mobile").is(":checked"),
                                agreed:"1"
                            }),
                            '_xsrf':$("input[name='_xsrf']").val()
                        },
                        success:function(dat){
                            if(dat.status=="true"){
                                window.location.replace("/success/"+dat.message)
                            }else{
                                $(".processing").css("display","none")
                                $("#processing-info").html("Failed to process."+dat.message)
                                $("#submit").html("Submit")
                            }
                        },
                        error:function(dat){
                            $(".processing").css("display","none")
                            $("#submit").html("Submit")
                            $("#processing-info").html(dat.message)
                        }
                    })
                }
            })
        });
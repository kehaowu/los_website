---
layout: default.html
---
## In order to be eligible for our study, you must be
1.  At least 18 years of age;
2.  Have no severe uncontrolled chronic diseases (for a detailed list of diseases, please check our [enrollment screening checklist](http://www.sph.tulane.edu/publichealth/bio/upload/03-Enrollment-screening-checklist_clean.pdf));
3.  (Female subjects only) not pregnant;
4.  (Female subjects only) have at least one intact ovary.
If you still have questions about your eligibility, please refer to our [enrollment screening checklist](http://www.sph.tulane.edu/publichealth/bio/upload/03-Enrollment-screening-checklist_clean.pdf), or call us at [504-988-1016](tel:5049881016).
5.  Please note, you can only participate our study once.

---
layout: default2.html
---
## What do we offer ?
1. A free bone assessment report.
2. $25 Walmart Gift Card.

## What will you be asked to do?
You will be invited to visit <a data-toggle="modal" data-target="#where"><u>our study center</u></a>. During your 2 hours visit in our center,
1. You will complete a [<u>medical history questionnaire</u>](http://www.sph.tulane.edu/publichealth/bio/upload/06-Questionnaire_Clean.pdf).
2. 20ml (2 tablespoons) blood will be collected from you.
3. Bone assessment using our standard bone densitometry machine (QDR 4500A).
4. You will receive a copy of your bone assessment report and $25 Walmart Giftcard.
You can refer to [<u>our consent form</u>](http://www.sph.tulane.edu/publichealth/bio/upload/Consent-Form_Clean.pdf) for more information.
---
layout: dafault.html
---

## FAQ
1. [What is our study ?](#whatisourstudy)
2. [What is osteoporosis ? ](#whatisosteoporosis)
3. [Why are we doing this study ?](#whyarewedoingthisstudy)
4. [What is the compensation ?](#whatisthecompensation)
5. [What is bone scan ?](#whatisbonescan)
6. [What is our report ?](#whatisourdexreport)

---bookmark whatisourstudy
# 1. What is our study?
The goal of this research study is to identify risk factors (including genetic and environmental) important for osteoporosis and related health problems. To reach this goal, we will recruit 20,000 human subjects in New Orleans and surrounding areas. 

Osteoporosis is a condition of low bone mass which results in high risk of fractures. It is a major public health problem. Osteoporosis is also associated with many other conditions, such as obesity, heart disease, and high blood pressure. The risk of osteoporosis has gender difference. While women have a relatively higher risk for developing osteoporosis comparing with men, men have a significantly higher mortality upon osteoporotic fractures. The most powerful, measurable determinant of fracture risk is bone mineral density (BMD). Osteoporosis is affected by both environmental factors (e.g., dietary calcium intake, alcohol consumption, smoking, weight-bearing exercise, etc.) and genetic factors.

Genetic factors involve genes. Genes are the factors people are born with, which determine traits such as hair and eye color. Genes can influence susceptibility to certain diseases. Genes are made up of DNA segments. DNA is reproducible in humans and can be transferred from parents to offspring. Proteins are the major products of genes. Different protein expression profiles between individuals may be caused by their genetic difference. 

A gene is a segment of DNA that carries out a specific function including bone metabolism. A gene functions by being made into its products, such as protein, by a process called expression. Researchers study genes and their functional products, such as proteins, in order to understand why some people have certain health problems, such as an inherently low BMD and thus a high risk of osteoporosis, and why some people have high BMD and thus a low risk of osteoporosis. Understanding protein expression may also be able to explain why some people respond to a treatment, while others do not, or why some people experience a side effect from a medication and others do not. 

This study is the first step in the process of osteoporosis research.  Eventual gene therapy could be developed to prevent or reduce the risk of developing osteoporosis and fractures. This could significantly reduce pain, morbidity, and mortality in millions of patients. It could also save the United States, and other countries, billions in health care dollars. 

---bookmark whatisosteoporosis
# 2. What is osteoporosis?
Osteoporosis is a disease of the bones that happens when you lose too much bone, make too little bone or both. As a result, your bones become weak and may break from a minor fall or, in serious cases, even from sneezing or bumping into furniture.
 ![The difference between a healthy bone and an osteoporotic bone]("img/osteoporosis.png")
<i>Figure 1. The difference between a healthy bone and an osteoporotic bone</i>

In the United States, as many as 9 million adults have osteoporosis, and an additional 48 million individuals have bone mass levels that put them at increased risk of developing osteoporosis.
There are a variety of factors - both controllable and uncontrollable - that put you at risk for developing osteoporosis.
<i>Table 1 The risk factors of osteoporosis</i>
---table-start
---table-head Uncontrollable Risk Factors 	|	Controllable Risk Factors
Being over age 50.	|	Not getting enough calcium and vitamin D.
Being female.	|	Not eating enough fruits and vegetables. 
Menopause.	|	Getting too much protein, sodium and caffeine. 
Family history of osteoporosis.		|	Having an inactive lifestyle.
Low body weight/being small and thin.		|	Smoking. 
Broken bones or height loss. 	|	Drinking too much alcohol.
......	|	Losing weight.
---table-end

Fractures are common and sometimes life-threatening. Bone fracture is not a simple event. Recovery from fracture could be slow, especially for those with osteoporosis. Body functions deteriorate during that time. Some people even develop life-threatening infection and clotting.
Dual-energy x-ray absorptiometry (DXA) is the best available clinical tool for the diagnosis of osteoporosis and for monitoring changes in bone mineral density (BMD) over time. Body scans of this proficiency have been limited in accessibility for a variety of reasons. A need for a prescription, lack of machine availability and costs typically exceeding $75 per scan are but a few. 
Here at Tulane, we care about your bone health, and we are proud to offer you a professional, free bone density test, so that you can take early actions to stay away from fracture.

---bookmark whyarewedoingthisstudy
# 3. Why are we doing this study ?
Current evidence shows that lack of physical exercise, certain diets and hormones, as well as certain concomitant diseases, may cause osteoporosis. However, genetic factors also play an important role, which we don’t fully understand.
 We aim to recruit 20,000 people. By analyzing their genetic information and bone density, we can better understand the mechanism of osteoporosis, so as to better prevent, diagnose, and treat osteoporosis.

---bookmark whatisthecompensation
# 4. What is the compensation ?
To thank you for your contribution to our study and to compensate you for your time and transportation expenses for this visit, you will receive a $25.00 gift card after the bone density test is completed. 
If you were determined to be ineligible for the study based on our follow-up screening result, you will still receive a $10.00 gift card as compensation.

---bookmark whatisbonescan
# 5. What is bone scan (DXA scan) ?
Dual-energy x-ray absorptiometry (DXA) is the best available clinical tool for the diagnosis of osteoporosis and for monitoring changes in bone mineral density (BMD) over time. The World Health Organization (WHO) has established a classification of BMD (using T-scores) that is the worldwide standard for the diagnosis of osteoporosis. Your doctor will take our study result seriously.
DXA scan involve exposure to a very small amount of radiation, approximately equal to that encountered during a brief exposure to sunlight. There will be no discomfort involved.
You can learn more about DXA scan from the link below:
1. Cleveland Clinic: [<u>https://my.clevelandclinic.org/services/imaging-institute/imaging-services/hic-dual-energy-xray-absorptiometry-dxa</u>](https://my.clevelandclinic.org/services/imaging-institute/imaging-services/hic-dual-energy-xray-absorptiometry-dxa)
2. Mayo Clinic: [<u>http://www.mayoclinic.org/tests-procedures/bone-density-test/basics/definition/prc-20020254</u>](http://www.mayoclinic.org/tests-procedures/bone-density-test/basics/definition/prc-20020254)
If you are interested, please do not hesitate to make an appointment with us online. If you have any questions, feel free to call us at 504-988-1016.

---bookmark whatisourdexreport
# 6. What is our study report ?
### Bone density report:
You will be able to estimate your fracture risk from our report. The T-score is the gold standard for diagnosing osteoporosis. You can also compare your bone strength to the national average.
![Sample bone density report]("img/report-BMD.jpg")
<i>Figure 2. Sample bone density report.</i>
### Body composition report:
You will also be given helpful information about your body composition.
![Sample body composition report with explanation]("img/report-body-composition.jpg")
<i>Figure 3. Sample body composition report with explanation.</i>


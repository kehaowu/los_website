# Louisiana Osteoporosis Study 预约系统

> 作者：吴珂皓 kehao.wu@gmail.com

## 其他
### 创建mysql账号
```shell
    mysql -uroot -p
```
MySQL 命令

```mysql
    insert into mysql.user(Host,User,Password) values("localhost","los",password(""));
    flush privileges;
    create database los;
    GRANT ALL PRIVILEGES ON los.* TO 'los'@'localhost' IDENTIFIED BY '' WITH GRANT OPTION;
    flush privileges;
```
```shell
    mysql -ulos los -p <sql/los_create.sql
    mv config/mysql.cfg.tpl config/mysql.cfg
    mv config/server.cfg.tpl config/server.cfg
```


#!/usr/bin/env python
#encoding=utf-8

import json,md5,uuid,datetime
from libs.base import *
from O365 import *
from libs.patient import patient

class registerHandler(BaseHandler):
    def post(self):
        dat = json.loads(self.get_argument("data",""))
        scode = json.load(file("config/server.cfg"))['scode']
        print scode
        print dat['scode']
        if scode != dat['scode']:
            self.write(json.dumps({'status':'false'}))
            return
        if dat:
            passwordmd5 = md5.new(dat['password'])
            password = passwordmd5.hexdigest()
            token = str(uuid.uuid1())
            print token
            info = self.db.query("SELECT * FROM admin WHERE username = %s", dat['username'])
            if info:
                print info
                status = "false"
                code = 1001
                message = "user existed"
            else:
                self.db.execute("INSERT INTO admin (username,password,token) VALUES(%s,%s,%s)",dat['username'],password,token)
                status = "true"
                code = 1000
                message = token
            print message
        else:
            status = "false"
            code = 1009
            message = "no data upload"
        self.write(json.dumps({
                'status':status,
                'code':code,
                'message':message
            }).encode('utf-8'))

class loginHandler(BaseHandler):
    def post(self):
        dat = json.loads(self.get_argument("data",""))
        print dat
        if dat:
            passwordmd5 = md5.new(dat['password'])
            password = passwordmd5.hexdigest()
            info = self.db.query("SELECT token FROM admin WHERE username = %s AND password = %s;",dat['username'],password)
            print info
            if info:
                token = str(uuid.uuid1())
                self.db.execute("UPDATE admin SET token = %s WHERE username = %s AND password = %s;",token,dat['username'],password)
                status = "true"
                code = 1000
                message = token
            else:
                status = "false"
                code = 1001
                message = "User not exist"
            print message
        else:
            status = "false"
            code = 1009
            message = "no data upload"
        self.write(json.dumps({
                'status':status,
                'code':code,
                'message':message
            }).encode('utf-8'))

class listHandler(BaseHandler):
    def getWeekday(self,date):
        wd = date.weekday()
        if  wd == 0:
            return " Mon"
        elif wd == 1:
            return " Tue"
        elif wd == 2:
            return " Wed"
        elif wd == 3:
            return " Thu"
        elif wd == 4:
            return " Fri"
        elif wd == 5:
            return " Sat"
        else:
            return " Sun"
    def get(self,all):
        dat = json.loads(self.get_argument("data",""))
        if dat:
            info = self.db.query("SELECT token FROM admin WHERE username = %s AND token = %s;",dat['username'],dat['token'])
            if info:
                status = "true"
                code = "1001"
                if all == "all":
                    info0 = self.db.query("SELECT * FROM timecard ORDER BY submit_time DESC LIMIT 100;")
                else:
                    info0 = self.db.query("SELECT * FROM timecard WHERE readed = 0;")
                    self.db.execute("UPDATE timecard SET readed = 1 WHERE readed = 0;")
                info = []
                for item in info0:
                    item['app_time'] = str(item['app_time'])
                    item['app_date'] = str(item['app_date']) + self.getWeekday(item['app_date'])
                    item['submit_time'] = str(item['submit_time'])
                    item['date_of_birth'] = str(item['date_of_birth'])
                    item['show_up_time'] = str(item['show_up_time'])
                    info.append(item)
            else:
                status = "false"
                code = "1002"
                info = "Access denied"
        else :
            status = "false"
            code = "1003"
            info = "No username"
        self.write(json.dumps(
            {
                'status':status,
                'code':code,
                'data':info
            }
            ).encode('utf-8'))

class confirmHandler(BaseHandler):
    def post(self):
        dat = json.loads(self.get_argument("data",""))
        print dat
        if dat:
            info = self.db.query("SELECT token FROM admin WHERE username = %s AND token = %s;",dat['username'],dat['token'])
            if info:
                status = "true"
                code = "1001"
                info = self.db.get("SELECT email,app_time,app_date,first_name,last_name,token FROM timecard WHERE id = %s;", dat['id'])
                print info
                authconfig = json.load(file("config/server.cfg"))
                authenticiation = (authconfig['O365']['email'],authconfig['O365']['password'])
                m = Message(auth=authenticiation)
                m.setRecipients(str(info['email']))  
                m.addRecipient("bonestudyneworleans@tulane.edu","LOS")
                m.setSubject("Louisiana Osteoporosis Study Appointment Registeration")
                content = "<h3>Hi "+info['first_name']+" "+info['last_name']+",</h3>"
                content = content + "<p style='text-indent:2em'>We have received your <a href='http://los.sph.tulane.edu/success/"+info['token']+"'>appointment request</a> (" + str(info['app_date']) + " " + str(info['app_time']) + ") for Louisiana Osteoporosis Study (Bone Study).</p>"
                content = content + "<p style='text-indent:2em'>We are writing to confirm your appointment. If you have any questions, please feel free to call us at 504-988-1016. Otherwise, we will see you on that day.<p>"
                content = content + "<p style='text-indent:2em'>Please <a href='http://los.sph.tulane.edu/#makeanappointment'>share us</a> with your friends.</p>"
                content = content + "<p style='text-align: right'>Louisiana Osteoporosis Study</p>"
                m.setBody(content)
                m.sendMessage()
                self.db.execute("UPDATE timecard SET show_up = 1 WHERE id = %s;", dat['id'])
            else:
                status = "false"
                code = "1002"
                info = "Access denied"
        else :
            status = "false"
            code = "1003"
            info = "No username"
        self.write(json.dumps({
                'status':status,
                'code':code
            }).encode('utf-8'))




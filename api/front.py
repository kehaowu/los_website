#!/usr/bin/env python
#encoding=utf-8

import json,time,datetime,uuid,md5
from libs.base import *
from O365 import *

class MakeAppointmentHandler(BaseHandler):
    def post(self):
        self.set_header('Content-Type', 'application/json; charset=UTF-8')
        self.screen("Request to make an appointment.")

        # Get json data
        dat = json.loads(self.get_argument('dat'))
        print dat
        # Init status
        status = "true"
        message = "OK"
        code = "1001"

        # Check Friday
        appTime = time.strptime(dat['date']+" "+dat['time'],"%Y-%m-%d %H:%M")
        if (appTime[3] >= 11 ) and (appTime[6] == 4):
            status = "false"
            code = "1004"
            message = "No appointment on Friday afternoon."
            self.screen(code+message)

        # Check date legal or not
        appointmentDay = time.strptime(dat['date'],"%Y-%m-%d")
        today = time.time()
        # Check date should be after tomorrow
        if (time.mktime(appointmentDay) - today) < 86400 :
            status = "false"
            code = "1003"
            message = "Today is not available."
            self.screen(code+message)

        # Check date weekend or not
        if (appointmentDay[6] == 5) or (appointmentDay[6] == 6):
            status = "false"
            code = "1004"
            message = "No appointment on weekend."
            self.screen(code+message)
        print status
        if status == "true":
            authconfig = json.load(file("config/server.cfg"))['O365']
            authenticiation = (authconfig['email'],authconfig['password'])
            schedule = Schedule(authenticiation)
            result = schedule.getCalendars()
            cal = schedule.calendars[0]
            time_string = '%Y-%m-%dT%H:%M:%SZ'
            start = time.strftime(dat['date']+"T00:00:00")
            end = time.strftime(dat['date']+"T23:30:00")
            self.screen("Query event.")
            result = cal.getEvents(start,end)
            unavailableTime = []
            availableTimeFormat = []

            #if len(cal.events) >= 6 and appTime[6] != 3:
            #    status = "false"
            #    message = "Not available."
            #    code = "1003"
            #if len(cal.events) >= 8 and appTime[6] == 3:
            #    status = "false"
            #    message = "Not available."
            #    code = "1003"

            if len(cal.events) >= 8:
                status = "false"
                message = "Not available."
                code = "1003"

            for event in cal.events:
                t1 = time.strptime(event.fullcalendarioJson()['start'],"%Y-%m-%dT%H:%M:%SZ")
                t2 = datetime.datetime(t1[0],t1[1],t1[2],t1[3]-timezone,t1[4],t1[5],t1[6])
                unavailableTime.append(t2)
            for item in self.getAvailableTime(appTime[3]):
                t2 = datetime.datetime(t1[0],t1[1],t1[2],item[0],item[1],0,t1[6])
                availableTimeFormat.append(t2)
            t1 = time.strptime(dat['date']+" "+dat['time'],"%Y-%m-%d %H:%M")
            appointmentTime = datetime.datetime(t1[0],t1[1],t1[2],t1[3],t1[4],t1[5],t1[6])
            if self.checkHoliday(time.strptime(dat['date'],"%Y-%m-%d")):
                status = "false"
                code = "1002"
                message = "Not available."
                self.screen(code+message)
            if not appointmentTime in availableTimeFormat:
                status = "false"
                code = "1002"
                message = "The time is not available."
                self.screen(code+message)
            if appointmentTime in unavailableTime:
                status = "false"
                code = "1003"
                message = "The time has already been selected by someone."
                self.screen(code+message)
            print status
            if status == "true":
                event = Event(auth=authenticiation,cal=cal)
                start = appointmentTime.strftime("%Y-%m-%dT%H:%M:%S")
                end = (appointmentTime+datetime.timedelta(minutes=30)).strftime("%Y-%m-%dT%H:%M:%S")
                event.setStart(start)
                event.setEnd(end)
                if dat['sms'] is True:
                    event.setSubject(dat['fname']+" "+dat['lname']+" "+dat['phone']+" Y"+" (Online)")
                else:
                    event.setSubject(dat['fname']+" "+dat['lname']+" "+dat['phone']+" (Online)")
                self.screen("upload appointment for"+dat['fname']+dat['lname']+dat['phone']+dat['email'])
                event.create()
                self.screen("upload appointment successfully for"+dat['fname']+dat['lname']+dat['phone']+dat['email'])
                tokenmd5 = md5.new(str(uuid.uuid1())+dat['fname']+dat['lname']+dat['dob'])
                token = tokenmd5.hexdigest()
                try:
                    self.screen("try to write record into database.")
                    info = self.db.execute("INSERT INTO timecard ( \
                    app_date, \
                    app_time, \
                    first_name, \
                    last_name, \
                    date_of_birth, \
                    phone, \
                    mobile_or_not, \
                    email, \
                    agreed, \
                    submit_time, \
                    show_up, \
                    operator, \
                    note, \
                    token \
                    )values( \
                    %s, \
                    %s, \
                    %s, \
                    %s, \
                    '1900-01-01', \
                    %s, \
                    %s, \
                    %s, \
                    %s, \
                    now(), \
                    '0', \
                    '', \
                    '', \
                    %s \
                    )",
                    dat['date'],
                    dat['time'],
                    dat['fname'],
                    dat['lname'],
                    dat['phone'],
                    dat['mobile'],
                    dat['email'],
                    dat['agreed'],
                    token,       
                    )
                    message = token
                    print info
                except:
                    self.screen("Failed to write.")
                    status = "false"
                    message = "Some error, please call us or click the contact us button."
                    self.screen(status+message)
        self.screen(status+message)
        self.write(json.dumps({
            'status':status,
            'code':code,
            'message':message
            }).encode('utf-8'))

class AvailableTimeHanlder(BaseHandler):
    def get(self):
        self.screen("Begin to lookup available time.")
        self.set_header('Content-Type', 'application/json; charset=UTF-8')
        date = self.get_argument('date')
        authconfig = json.load(file("config/server.cfg"))['O365']
        appointmentDay = time.strptime(date,"%Y-%m-%d")
        today = time.time()
        status = "true"
        message = "ok"
        code = "1001"
        dat = []
        if self.checkHoliday(appointmentDay):
            status = "false"
            code = "1002"
            message = "Not available."
            self.screen(code+message)
        if (time.mktime(appointmentDay) - today) < 86400 :
            status = "false"
            code = "1002"
            message = "Today is not available."
            self.screen(code+message)
        if (appointmentDay[6] == 5) or (appointmentDay[6] == 6):
            status = "false"
            code = "1003"
            message = "No appointment on weekend."
            self.screen(code+message)
        availableTime = self.getAvailableTime(appointmentDay[6])
        if status == "true":
            authenticiation = (authconfig['email'],authconfig['password'])
            schedule = Schedule(authenticiation)
            result = schedule.getCalendars()
            cal = schedule.calendars[0]
            time_string = '%Y-%m-%dT%H:%M:%SZ'
            start = time.strftime(date+"T00:00:00")
            end = time.strftime(date+"T23:30:00")
            result = cal.getEvents(start,end)
            unavailableTime = []
            availableTimeFormat = []
            print(cal.events)
            if len(cal.events) >= 8:
                status = "false"
                message = "Not available."
                code = "1003"
            else:
                for event in cal.events:
                    t1 = time.strptime(event.fullcalendarioJson()['start'],"%Y-%m-%dT%H:%M:%SZ")
                    print t1
                    t2 = datetime.datetime(t1[0],t1[1],t1[2],t1[3]-timezone,t1[4],t1[5],t1[6])
                    unavailableTime.append(t2)
                for item in availableTime:
                    t2 = datetime.datetime(t1[0],t1[1],t1[2],item[0],item[1],0,t1[6])
                    availableTimeFormat.append(t2)
                avaiTime = list(set(availableTimeFormat).difference(set(unavailableTime)))
                for avaiTimeItem in avaiTime:
                    dat.append(avaiTimeItem.strftime('%H:%M'))
                    dat = sorted(dat)
        self.screen(code+message)
        self.write(json.dumps({
            'status':status,
            'message':message,
            'code':code,
            'data':dat
            }).encode('utf-8'))

class AvailableDateHandler(BaseHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json; charset=UTF-8')
        date = self.get_argument('date')
        appointmentDay = time.strptime(date,"%Y-%m-%d")
        today = time.time()
        status = "true"
        message = "ok"
        code = "1001"
        dat = []
        if (time.mktime(appointmentDay) - today) < 86400 :
            status = "false"
            code = "1002"
            message = "Today is not available."
        if (appointmentDay[6] == 5) or (appointmentDay[6] == 6):
            status = "false"
            code = "1003"
            message = "No appointment on weekend."
        self.checkDateAvailable(appointmentDay)
        self.write(status)

class EmailUsHandler(BaseHandler):
    def post(self):
        dat = json.loads(self.get_argument('dat'))
        #data = json.loads(self.get_argument('data',"A"))
        print dat
        authconfig = json.load(file("config/server.cfg"))
        authenticiation = (authconfig['O365']['email'],authconfig['O365']['password'])
        m = Message(auth=authenticiation)
        for recipient in authconfig['recipients']:
            print recipient
            if m.getRecipientsEmail() == {}:
                m.setRecipients(str(authconfig['recipients'][recipient]['EmailAddress']['Address']))
            else:
                m.addRecipient(authconfig['recipients'][recipient]['EmailAddress']['Address'],\
                    authconfig['recipients'][recipient]['EmailAddress']['Name'])
        m.setSubject("LOS FAQ from " + dat['name'])
        m.setBody("From: "+dat['email'] +"\n" + dat['content'] + "\n")
        m.sendMessage()
        self.write(json.dumps({
            'status':'true',
            }).encode('utf-8'))


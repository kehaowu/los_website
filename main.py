#!/usr/bin/env python
import os
import base64, uuid
import tornado.ioloop
import tornado.web
import tornado.httpserver
import tornado.options
import torndb
import tornado

from tornado.web import RequestHandler
from tornado.web import traceback
from tornado.options import define, options

from libs.base import BaseHandler

from libs.config.getConfig import MysqlConfig
from libs import pages,markdown
from api import front,admin
from O365 import *

define("port", default=3381, help="run on the given port", type=int)

# cookie_sec = base64.b64encode(uuid.uuid4().bytes + uuid.uuid4().bytes)
cookie_sec = 'yHKPTuoj9KEmGSc0R4TSyiKzOScTvTfy'

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
                    ### Front-end
                    (r"/", pages.indexHandler),
                    (r"/success/(\w+)", pages.successHandler),
                    (r"/contact", pages.contactHandler),
                    (r"/misc/(\w+)", pages.miscHandler),
                    (r"/admin", pages.adminHandler),
                    (r"/admin/register", pages.registerHandler),
                    (r"/admin/login", pages.loginHandler),

                    (r"/api/makeappointment", front.MakeAppointmentHandler),
                    (r"/api/emailus", front.EmailUsHandler),
                    (r"/api/availabletime", front.AvailableTimeHanlder),
                    (r"/api/availabledate", front.AvailableDateHandler),
                    (r"/api/admin/register", admin.registerHandler),
                    (r"/api/admin/login", admin.loginHandler),
                    (r"/api/admin/list/(\w+)", admin.listHandler),
                    (r"/api/admin/confirm", admin.confirmHandler),
                    
                    (r".*", pages.errorHandler),

                    ]
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "static/html/1/"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            ui_modules={
                'share':pages.share,
            },
            xsrf_cookies=False,
            cookie_secret=cookie_sec,
            debug=True,
        )
        super(Application,self).__init__(handlers, **settings)
        mysqlcfg = MysqlConfig('main')
        self.db = torndb.Connection(
                host='%s:%s'%(mysqlcfg.host,mysqlcfg.port), database=mysqlcfg.database,
                user=mysqlcfg.user, password=mysqlcfg.passwd, 
                time_zone=mysqlcfg.time_zone, charset=mysqlcfg.charset)

if __name__ == "__main__":
    print 'START RUNNING SERVER ON PORT ' + str(options.port)
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    markdown.markdown(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.current().start()



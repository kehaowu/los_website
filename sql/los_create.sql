-- tables
-- Table timecard
CREATE TABLE timecard (
    id int    NOT NULL  AUTO_INCREMENT,
    app_date date    NULL ,
    app_time time    NULL ,
    first_name varchar(100)    NULL ,
    last_name varchar(100)    NULL ,
    date_of_birth date    NULL ,
    phone varchar(20)    NULL ,
    mobile_or_not int    DEFAULT 0,
    email varchar(255)    NOT NULL ,
    agreed int    NULL DEFAULT 0 ,
    submit_time datetime    NOT NULL ,
    show_up int    NULL ,
    show_up_time time    NULL ,
    operator varchar(10)    NOT NULL ,
    note text    NOT NULL ,
    token varchar(255) NOT NULL,
    readed int DEFAULT 0,
    CONSTRAINT timecard_pk PRIMARY KEY (id)
);

CREATE TABLE admin (
    id int NOT NULL AUTO_INCREMENT,
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    token varchar(255) NOT NULL,
    CONSTRAINT admin_pk PRIMARY KEY (id)
)




-- End of file.


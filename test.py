import datetime
from libs.patient import patient

days = 365

today = datetime.date.today()
queryday = today
for i in range(days):
    querydaystring = datetime.datetime.strftime(queryday,'%Y-%m-%d')
    print "\033[1;31;40mQuerying for",querydaystring,"\033[1;31;0m"
    p = patient()
    p.set_date(querydaystring)
    p.fetch()
    p.write()
    queryday = queryday - datetime.timedelta(1)
